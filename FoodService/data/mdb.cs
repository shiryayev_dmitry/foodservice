﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FoodService
{
    public class mdb
    {
        public OleDbConnection g_connection;
        /// <summary>
        /// Установить соединение
        /// </summary>
        /// <param name="path"></param>
        public void DoConnected(string path)
        {
            try
            {
                g_connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\"" +
                                 path + "\"; Jet OLEDB:Database Password=\"\";" +
                                 "Jet OLEDB:Engine Type=5;Jet OLEDB:Encrypt Database=True;");
                g_connection.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Произошла ошибка в момент установления соединения с бд: " + ex.Message);
            }
        }
        /// <summary>
        /// Зарегистрировать продукцию
        /// </summary>
        /// <param name="foodParams"></param>
        public void DoRegistryProducts(FoodParams foodParams)
        {
            using (OleDbCommand cmd = g_connection.CreateCommand())
            {
                cmd.CommandText = @"INSERT INTO FoodService 
                    (name, food_count, date_in, realization_time, food_type) VALUES (?,?,?,?,?)";
                cmd.Parameters.AddWithValue("name", foodParams.Name);
                cmd.Parameters.AddWithValue("food_count", foodParams.FoodCount);
                cmd.Parameters.AddWithValue("date_in", foodParams.DateIn);
                cmd.Parameters.AddWithValue("realization_time",foodParams.RealizationTime);
                cmd.Parameters.AddWithValue("food_type", (int)foodParams.FoodType);
                cmd.ExecuteNonQuery();
            }
        }
        /// <summary>
        /// Изменить параметры продукции
        /// </summary>
        /// <param name="food"></param>
        public void DoChangeParams(FoodParams food)
        {
            using (OleDbCommand cmd = g_connection.CreateCommand())
            {
                cmd.CommandText = @"UPDATE FoodService SET 
                    name=@name, food_count=@food_count, date_in=@date_in, realization_time=@realization_time,
                    food_type=@food_type, date_end=@date_end WHERE food_id=@food_id";
                cmd.Parameters.AddWithValue("name", food.Name);
                cmd.Parameters.AddWithValue("food_count", food.FoodCount);
                cmd.Parameters.AddWithValue("date_in", food.DateIn);
                cmd.Parameters.AddWithValue("realization_time", food.RealizationTime);
                cmd.Parameters.AddWithValue("food_type", (int)food.FoodType);
                if(food.DateEnd != new DateTime())
                    cmd.Parameters.AddWithValue("date_end", food.DateEnd);
                else
                    cmd.Parameters.AddWithValue("date_end", DBNull.Value);
                cmd.Parameters.AddWithValue("food_id", food.Id);

                cmd.ExecuteNonQuery();
            }
        }
        /// <summary>
        /// Реализовать продукцию
        /// </summary>
        /// <param name="foodCount"></param>
        /// <param name="id"></param>
        /// <param name="dateEnd"></param>
        public void DoRealizeProduct(double foodCount, int id, DateTime dateEnd = new DateTime())
        {
            using (OleDbCommand cmd = g_connection.CreateCommand())
            {
                
                cmd.CommandText = @"UPDATE FoodService SET food_count=@food_count, date_end=@dateEnd WHERE food_id=@food_id";
                cmd.Parameters.AddWithValue("food_count", foodCount);
                if (dateEnd != new DateTime())
                    cmd.Parameters.AddWithValue("date_end", dateEnd.ToString("G"));
                else
                    cmd.Parameters.AddWithValue("date-end", DBNull.Value);
                cmd.Parameters.AddWithValue("food_id", id);
                cmd.ExecuteNonQuery();
            }
        }
        /// <summary>
        /// Получить продукт по Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public FoodParams GetFoodById(int id)
        {
            FoodParams foodParams = new FoodParams();
            using (OleDbCommand cmd = g_connection.CreateCommand())
            {
                cmd.CommandText = "SELECT name, food_count, date_in, realization_time, food_type, date_end, food_id FROM FoodService WHERE food_id=@food_id";
                cmd.Parameters.AddWithValue("food_id", id);
                using (OleDbDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        foodParams.Name = reader.GetString(0);
                        foodParams.FoodCount = reader.GetDouble(1);
                        foodParams.DateIn = reader.GetDateTime(2);
                        foodParams.RealizationTime = reader.GetDateTime(3);
                        foodParams.FoodType = (FoodType)reader.GetInt32(4);
                        if (!reader.IsDBNull(5))
                        {
                            foodParams.DateEnd = reader.GetDateTime(5);
                        }
                        foodParams.Id = reader.GetInt32(6);
                    }
                }
            }
            return foodParams;
        }
        /// <summary>
        /// Получить всю продукцию за день
        /// </summary>
        /// <returns></returns>
        public List<FoodParams> GetFoodsByDay()
        {
            List<FoodParams> foodParamsList = new List<FoodParams>();
            using (OleDbCommand cmd = g_connection.CreateCommand())
            {
                cmd.CommandText = "SELECT name, food_count, date_in, realization_time, food_type, date_end, food_id FROM FoodService";
                using (OleDbDataReader reader = cmd.ExecuteReader())
                {
                    while(reader.Read())
                    {
                        FoodParams foodParams = new FoodParams();
                        foodParams.Name = reader.GetString(0);
                        foodParams.FoodCount = reader.GetDouble(1);
                        foodParams.DateIn = reader.GetDateTime(2);
                        foodParams.RealizationTime = reader.GetDateTime(3);
                        foodParams.FoodType = (FoodType)reader.GetInt32(4);
                        if(!reader.IsDBNull(5))
                        {
                            foodParams.DateEnd = reader.GetDateTime(5);
                        }
                        foodParams.Id = reader.GetInt32(6);
                        //Добавить только за сегодняшний день
                        if (foodParams.DateIn.Day == DateTime.Now.Day &&
                            foodParams.DateIn.Month == DateTime.Now.Month &&
                            foodParams.DateIn.Year == DateTime.Now.Year)
                        {
                            foodParamsList.Add(foodParams);
                        }
                            
                    }
                }
            }
            return foodParamsList;
        }
        /// <summary>
        /// Продукция в реализации
        /// </summary>
        /// <returns></returns>
        public List<FoodParams> GetFoodOnStore()
        {
            List<FoodParams> foodParamsList = new List<FoodParams>();
            using (OleDbCommand cmd = g_connection.CreateCommand())
            {
                cmd.CommandText = @"SELECT name, food_count, date_in, realization_time, food_type, food_id FROM FoodService
                                    WHERE date_end IS NULL";
                using (OleDbDataReader reader = cmd.ExecuteReader())
                {
                    while(reader.Read())
                    {
                        FoodParams foodParams = new FoodParams();
                        foodParams.Name = reader.GetString(0);
                        foodParams.FoodCount = reader.GetDouble(1);
                        foodParams.DateIn = reader.GetDateTime(2);
                        foodParams.RealizationTime = reader.GetDateTime(3);
                        foodParams.FoodType = (FoodType)reader.GetInt32(4);
                        foodParams.Id = reader.GetInt32(5);
                        foodParamsList.Add(foodParams);
                    }
                }
            }
            return foodParamsList;
        }
    }
}
