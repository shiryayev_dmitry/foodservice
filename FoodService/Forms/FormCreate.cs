﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FoodService
{
    public partial class FormCreate : Form
    {
        mdb mdb;
        public FormCreate(mdb _mdb)
        {
            InitializeComponent();
            mdb = _mdb;
            //Присвоить стартовое значение количеству
            tbCount.Text = "0";
            //Присвоить стартовый тип продукции
            cbType.SelectedIndex = 0;
        }

        //Добавить продукцию
        private void btnCreate_Click(object sender, EventArgs e)
        {
            FoodParams foodParams = new FoodParams();
            foodParams.Name = tbName.Text;
            foodParams.DateIn = DateTime.Parse(String.Format("{0} {1}",
                dtpDate.Value.ToShortDateString(),dtpTime.Value.ToShortTimeString()));
            foodParams.RealizationTime = dtpTimeRealization.Value;
            foodParams.FoodCount = double.Parse(tbCount.Text);
            foodParams.FoodType = (FoodType)cbType.SelectedIndex;

            mdb.DoRegistryProducts(foodParams);

            if(MessageBox.Show("Добавить ещё продукции?", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                lbName.Text = "";
                tbCount.Text = "0";
                cbType.SelectedIndex = 0;
            }
            else
            {
                this.Close();
            }
        }
    }
}
