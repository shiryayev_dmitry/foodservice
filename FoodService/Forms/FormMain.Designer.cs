﻿namespace FoodService
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvFoodNow = new System.Windows.Forms.DataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Count = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DateIn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DateResolve = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnCreat = new System.Windows.Forms.Button();
            this.btnRealize = new System.Windows.Forms.Button();
            this.btnController = new System.Windows.Forms.Button();
            this.LTime = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFoodNow)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvFoodNow
            // 
            this.dgvFoodNow.AllowUserToAddRows = false;
            this.dgvFoodNow.AllowUserToOrderColumns = true;
            this.dgvFoodNow.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvFoodNow.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvFoodNow.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvFoodNow.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dgvFoodNow.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFoodNow.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.Name,
            this.Count,
            this.DateIn,
            this.DateResolve});
            this.dgvFoodNow.Cursor = System.Windows.Forms.Cursors.Default;
            this.dgvFoodNow.Location = new System.Drawing.Point(13, 41);
            this.dgvFoodNow.Name = "dgvFoodNow";
            this.dgvFoodNow.ReadOnly = true;
            this.dgvFoodNow.RowHeadersVisible = false;
            this.dgvFoodNow.RowTemplate.Height = 24;
            this.dgvFoodNow.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvFoodNow.Size = new System.Drawing.Size(639, 397);
            this.dgvFoodNow.TabIndex = 0;
            // 
            // Id
            // 
            this.Id.FillWeight = 20F;
            this.Id.HeaderText = "№";
            this.Id.Name = "Id";
            this.Id.ReadOnly = true;
            // 
            // Name
            // 
            this.Name.HeaderText = "Наименование";
            this.Name.Name = "Name";
            this.Name.ReadOnly = true;
            // 
            // Count
            // 
            this.Count.FillWeight = 40F;
            this.Count.HeaderText = "на складе";
            this.Count.Name = "Count";
            this.Count.ReadOnly = true;
            // 
            // DateIn
            // 
            this.DateIn.HeaderText = "Дата поступления";
            this.DateIn.Name = "DateIn";
            this.DateIn.ReadOnly = true;
            // 
            // DateResolve
            // 
            this.DateResolve.HeaderText = "Конечное время на реализацию";
            this.DateResolve.Name = "DateResolve";
            this.DateResolve.ReadOnly = true;
            // 
            // btnCreat
            // 
            this.btnCreat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCreat.Location = new System.Drawing.Point(658, 41);
            this.btnCreat.Name = "btnCreat";
            this.btnCreat.Size = new System.Drawing.Size(227, 23);
            this.btnCreat.TabIndex = 1;
            this.btnCreat.Text = "Добавить продукцию";
            this.btnCreat.UseVisualStyleBackColor = true;
            this.btnCreat.Click += new System.EventHandler(this.btnCreat_Click);
            // 
            // btnRealize
            // 
            this.btnRealize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRealize.Location = new System.Drawing.Point(658, 70);
            this.btnRealize.Name = "btnRealize";
            this.btnRealize.Size = new System.Drawing.Size(227, 23);
            this.btnRealize.TabIndex = 2;
            this.btnRealize.Text = "Реализовать продукцию";
            this.btnRealize.UseVisualStyleBackColor = true;
            this.btnRealize.Click += new System.EventHandler(this.btnRealize_Click);
            // 
            // btnController
            // 
            this.btnController.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnController.Location = new System.Drawing.Point(658, 99);
            this.btnController.Name = "btnController";
            this.btnController.Size = new System.Drawing.Size(227, 23);
            this.btnController.TabIndex = 3;
            this.btnController.Text = "Управляющий терминал";
            this.btnController.UseVisualStyleBackColor = true;
            this.btnController.Click += new System.EventHandler(this.btnController_Click);
            // 
            // LTime
            // 
            this.LTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.LTime.AutoSize = true;
            this.LTime.Location = new System.Drawing.Point(658, 421);
            this.LTime.Name = "LTime";
            this.LTime.Size = new System.Drawing.Size(120, 17);
            this.LTime.TabIndex = 4;
            this.LTime.Text = "01.06.2019 00:01";
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(897, 450);
            this.Controls.Add(this.LTime);
            this.Controls.Add(this.btnController);
            this.Controls.Add(this.btnRealize);
            this.Controls.Add(this.btnCreat);
            this.Controls.Add(this.dgvFoodNow);
            this.ShowIcon = false;
            this.Text = "FoodService";
            ((System.ComponentModel.ISupportInitialize)(this.dgvFoodNow)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvFoodNow;
        private System.Windows.Forms.Button btnCreat;
        private System.Windows.Forms.Button btnRealize;
        private System.Windows.Forms.Button btnController;
        private System.Windows.Forms.Label LTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Count;
        private System.Windows.Forms.DataGridViewTextBoxColumn DateIn;
        private System.Windows.Forms.DataGridViewTextBoxColumn DateResolve;
    }
}