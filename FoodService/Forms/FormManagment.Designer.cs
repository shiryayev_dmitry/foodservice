﻿namespace FoodService
{
    partial class FormManagment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvFoodNow = new System.Windows.Forms.DataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Count = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DateIn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TimeRealize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DateEnd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnAll = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFoodNow)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvFoodNow
            // 
            this.dgvFoodNow.AllowUserToAddRows = false;
            this.dgvFoodNow.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvFoodNow.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvFoodNow.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFoodNow.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.Name,
            this.Count,
            this.DateIn,
            this.TimeRealize,
            this.DateEnd});
            this.dgvFoodNow.Cursor = System.Windows.Forms.Cursors.Default;
            this.dgvFoodNow.Location = new System.Drawing.Point(12, 72);
            this.dgvFoodNow.Name = "dgvFoodNow";
            this.dgvFoodNow.RowHeadersVisible = false;
            this.dgvFoodNow.RowTemplate.Height = 24;
            this.dgvFoodNow.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvFoodNow.Size = new System.Drawing.Size(776, 414);
            this.dgvFoodNow.TabIndex = 1;
            this.dgvFoodNow.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvFoodNow_CellContentDoubleClick);
            // 
            // Id
            // 
            this.Id.HeaderText = "№";
            this.Id.Name = "Id";
            // 
            // Name
            // 
            this.Name.HeaderText = "Наименование";
            this.Name.Name = "Name";
            this.Name.ReadOnly = true;
            // 
            // Count
            // 
            this.Count.HeaderText = "Количество на складе";
            this.Count.Name = "Count";
            this.Count.ReadOnly = true;
            // 
            // DateIn
            // 
            this.DateIn.HeaderText = "Дата поступления";
            this.DateIn.Name = "DateIn";
            // 
            // TimeRealize
            // 
            this.TimeRealize.HeaderText = "Время на реализацию";
            this.TimeRealize.Name = "TimeRealize";
            // 
            // DateEnd
            // 
            this.DateEnd.HeaderText = "Дата выхода";
            this.DateEnd.Name = "DateEnd";
            // 
            // btnAll
            // 
            this.btnAll.Location = new System.Drawing.Point(575, 29);
            this.btnAll.Name = "btnAll";
            this.btnAll.Size = new System.Drawing.Size(212, 37);
            this.btnAll.TabIndex = 2;
            this.btnAll.Text = "вся продукция за день";
            this.btnAll.UseVisualStyleBackColor = true;
            this.btnAll.Click += new System.EventHandler(this.btnAll_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(312, 29);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(232, 37);
            this.button1.TabIndex = 3;
            this.button1.Text = "Продукция в использовании";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // FormManagment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 498);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnAll);
            this.Controls.Add(this.dgvFoodNow);
            //this.Name = "FormManagment";
            this.ShowIcon = false;
            this.Text = "Управляющий терминал";
            ((System.ComponentModel.ISupportInitialize)(this.dgvFoodNow)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvFoodNow;
        private System.Windows.Forms.Button btnAll;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Count;
        private System.Windows.Forms.DataGridViewTextBoxColumn DateIn;
        private System.Windows.Forms.DataGridViewTextBoxColumn TimeRealize;
        private System.Windows.Forms.DataGridViewTextBoxColumn DateEnd;
    }
}