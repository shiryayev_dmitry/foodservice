﻿namespace FoodService
{
    partial class FormChange
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dtpRealize = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.tbCount = new System.Windows.Forms.TextBox();
            this.cbType = new System.Windows.Forms.ComboBox();
            this.dtpTime = new System.Windows.Forms.DateTimePicker();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.tbName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lbName = new System.Windows.Forms.Label();
            this.btnCreate = new System.Windows.Forms.Button();
            this.dtpTimeEnd = new System.Windows.Forms.DateTimePicker();
            this.dtpDataEnd = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // dtpRealize
            // 
            this.dtpRealize.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpRealize.Location = new System.Drawing.Point(176, 121);
            this.dtpRealize.Name = "dtpRealize";
            this.dtpRealize.Size = new System.Drawing.Size(111, 22);
            this.dtpRealize.TabIndex = 23;
            this.dtpRealize.Value = new System.DateTime(2019, 6, 1, 0, 0, 0, 0);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 121);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(155, 17);
            this.label4.TabIndex = 22;
            this.label4.Text = "Время на реализацию";
            // 
            // tbCount
            // 
            this.tbCount.Location = new System.Drawing.Point(374, 81);
            this.tbCount.Name = "tbCount";
            this.tbCount.Size = new System.Drawing.Size(98, 22);
            this.tbCount.TabIndex = 21;
            // 
            // cbType
            // 
            this.cbType.FormattingEnabled = true;
            this.cbType.Items.AddRange(new object[] {
            "кг",
            "шт",
            "л"});
            this.cbType.Location = new System.Drawing.Point(176, 78);
            this.cbType.Name = "cbType";
            this.cbType.Size = new System.Drawing.Size(111, 24);
            this.cbType.TabIndex = 20;
            // 
            // dtpTime
            // 
            this.dtpTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpTime.Location = new System.Drawing.Point(293, 44);
            this.dtpTime.Name = "dtpTime";
            this.dtpTime.Size = new System.Drawing.Size(100, 22);
            this.dtpTime.TabIndex = 19;
            // 
            // dtpDate
            // 
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDate.Location = new System.Drawing.Point(176, 44);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(111, 22);
            this.dtpDate.TabIndex = 18;
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(176, 12);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(296, 22);
            this.tbName.TabIndex = 17;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(70, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 17);
            this.label3.TabIndex = 16;
            this.label3.Text = "Дата прихода";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(290, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 17);
            this.label2.TabIndex = 15;
            this.label2.Text = "Колиество";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(137, 81);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 17);
            this.label1.TabIndex = 14;
            this.label1.Text = "Тип";
            // 
            // lbName
            // 
            this.lbName.AutoSize = true;
            this.lbName.Location = new System.Drawing.Point(64, 12);
            this.lbName.Name = "lbName";
            this.lbName.Size = new System.Drawing.Size(106, 17);
            this.lbName.TabIndex = 13;
            this.lbName.Text = "Наименование";
            // 
            // btnCreate
            // 
            this.btnCreate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCreate.Location = new System.Drawing.Point(447, 195);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(86, 23);
            this.btnCreate.TabIndex = 12;
            this.btnCreate.Text = "Изменить";
            this.btnCreate.UseVisualStyleBackColor = true;
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // dtpTimeEnd
            // 
            this.dtpTimeEnd.Enabled = false;
            this.dtpTimeEnd.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpTimeEnd.Location = new System.Drawing.Point(293, 160);
            this.dtpTimeEnd.Name = "dtpTimeEnd";
            this.dtpTimeEnd.Size = new System.Drawing.Size(100, 22);
            this.dtpTimeEnd.TabIndex = 26;
            // 
            // dtpDataEnd
            // 
            this.dtpDataEnd.Enabled = false;
            this.dtpDataEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDataEnd.Location = new System.Drawing.Point(176, 160);
            this.dtpDataEnd.Name = "dtpDataEnd";
            this.dtpDataEnd.Size = new System.Drawing.Size(111, 22);
            this.dtpDataEnd.TabIndex = 25;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(45, 163);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(125, 17);
            this.label5.TabIndex = 24;
            this.label5.Text = "Дата реализации";
            this.label5.Visible = false;
            // 
            // FormChange
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(545, 230);
            this.Controls.Add(this.dtpTimeEnd);
            this.Controls.Add(this.dtpDataEnd);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.dtpRealize);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbCount);
            this.Controls.Add(this.cbType);
            this.Controls.Add(this.dtpTime);
            this.Controls.Add(this.dtpDate);
            this.Controls.Add(this.tbName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbName);
            this.Controls.Add(this.btnCreate);
            this.Name = "FormChange";
            this.ShowIcon = false;
            this.Text = "Изменить данные";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dtpRealize;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbCount;
        private System.Windows.Forms.ComboBox cbType;
        private System.Windows.Forms.DateTimePicker dtpTime;
        private System.Windows.Forms.DateTimePicker dtpDate;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbName;
        private System.Windows.Forms.Button btnCreate;
        private System.Windows.Forms.DateTimePicker dtpTimeEnd;
        private System.Windows.Forms.DateTimePicker dtpDataEnd;
        private System.Windows.Forms.Label label5;
    }
}