﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FoodService
{
    public partial class FormManagment : Form
    {
        mdb mdb;
        public FormManagment(mdb _mdb)
        {
            InitializeComponent();
            mdb = _mdb;
            GetFoodsByDay();
        }

        public void GetFoodsByDay()
        {
            List<FoodParams> foods = mdb.GetFoodsByDay();
            dgvFoodNow.Rows.Clear();
            int index = 0;

            foreach (FoodParams food in foods)
            {
                dgvFoodNow.Rows.Add();
                dgvFoodNow.Rows[index].Cells["Id"].Value = food.Id;
                dgvFoodNow.Rows[index].Cells["Name"].Value = food.Name;
                dgvFoodNow.Rows[index].Cells["Count"].Value = food.FoodCount.ToString();
                dgvFoodNow.Rows[index].Cells["DateIn"].Value = food.DateIn.ToString("G");
                dgvFoodNow.Rows[index].Cells["TimeRealize"].Value = food.RealizationTime.ToLongTimeString();
                if (food.DateEnd != new DateTime())
                    dgvFoodNow.Rows[index].Cells["DateEnd"].Value = food.DateEnd.ToString("G");

                index++;
            }
        }

        public void GetFoodsNow()
        {
            List<FoodParams> foods = mdb.GetFoodOnStore();
            dgvFoodNow.Rows.Clear();
            int index = 0;

            foreach (FoodParams food in foods)
            {
                dgvFoodNow.Rows.Add();
                dgvFoodNow.Rows[index].Cells["Id"].Value = food.Id;
                dgvFoodNow.Rows[index].Cells["Name"].Value = food.Name;
                dgvFoodNow.Rows[index].Cells["Count"].Value = food.FoodCount.ToString();
                dgvFoodNow.Rows[index].Cells["DateIn"].Value = food.DateIn.ToString("G");
                dgvFoodNow.Rows[index].Cells["TimeRealize"].Value = food.RealizationTime.ToLongTimeString();

                index++;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            GetFoodsNow();
        }

        private void btnAll_Click(object sender, EventArgs e)
        {
            GetFoodsByDay();
        }

        private void dgvFoodNow_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int id = Int32.Parse(dgvFoodNow.CurrentRow.Cells[0].Value.ToString());
            FoodParams food = mdb.GetFoodById(id);
            FormChange fChange = new FormChange(food, mdb);
            fChange.ShowDialog();
            GetFoodsByDay();
        }
    }
}
