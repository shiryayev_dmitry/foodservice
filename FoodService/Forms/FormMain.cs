﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FoodService
{
    public partial class FormMain : Form
    {
        Thread thread = null;
        /// <summary>
        /// Динамическое текущее время
        /// </summary>
        System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
        /// <summary>
        /// Связь с бд
        /// </summary>
        mdb mdb = new mdb();
        public FormMain()
        {
            InitializeComponent();
            //Подключение к бд
            mdb.DoConnected("../../../FoodService.mdb");
            //Установление динамического изменения времени
            timer.Tick += new EventHandler(RefreshLabel);
            //Установка интервала
            timer.Interval = 1000;
            timer.Start();
            UpdateFoodParams();
        }

        //Динамически обновлять время
        private void RefreshLabel(object sender, EventArgs e)
        {
            LTime.Text = DateTime.Now.ToString("G");
        }
        //Создать задачу
        private void btnCreat_Click(object sender, EventArgs e)
        {
            FormCreate fCreate = new FormCreate(mdb);
            fCreate.ShowDialog();
            UpdateFoodParams();
        }
        //Уведомить о том, что у продукта вышел срок годности
        private void DoNotification(int index, string name)
        {
            dgvFoodNow.Rows[index].DefaultCellStyle.BackColor = Color.Red;
            MessageBox.Show(name + " - вышел срок годности");
        }
        /// <summary>
        /// Обновить параметры
        /// </summary>
        private void UpdateFoodParams()
        {
            List<FoodParams> foodList = mdb.GetFoodOnStore();
            dgvFoodNow.Rows.Clear();
            int index = 0;
            if(thread !=null)
                thread.Abort();

            foreach(FoodParams food in foodList)
            {
                dgvFoodNow.Rows.Add();
                dgvFoodNow.Rows[index].Cells["Id"].Value = food.Id;
                dgvFoodNow.Rows[index].Cells["Name"].Value = food.Name;
                dgvFoodNow.Rows[index].Cells["Count"].Value = food.FoodCount.ToString();
                dgvFoodNow.Rows[index].Cells["DateIn"].Value = food.DateIn.ToString("G");

                dgvFoodNow.Rows[index].Cells["DateResolve"].Value = string.Format("{0} {1}:{2}:{3}",
                    food.DateIn.ToShortDateString(), (food.DateIn.Hour + food.RealizationTime.Hour),
                    (food.DateIn.Minute + food.RealizationTime.Minute),
                    (food.DateIn.Second + food.RealizationTime.Second));
                int id = index;
                //Уведомить пользователя через N время
                Task.Factory.StartNew(() =>
                    {
                        thread = Thread.CurrentThread;
                        int time = GetWaitSeconds(food.DateIn, food.RealizationTime);
                        //Если время ещё актуально, задать таймер
                        if(time > 0)
                            Thread.Sleep(time);
                        //уведомить
                        DoNotification(id, food.Name);
                    }
                );
                index++;
            }
        }
        //Получить через сколько секунд уведомить
        private int GetWaitSeconds(DateTime dateIn, DateTime realizeDate)
        {
            int hour = (dateIn.Hour + realizeDate.Hour) - DateTime.Now.Hour;
            int minute = (dateIn.Minute + realizeDate.Minute) - DateTime.Now.Minute;
            int sec = (dateIn.Second + realizeDate.Second) - DateTime.Now.Second;
            return sec * 1000 + minute * 60000 + hour * 3600000;
        }
        //Реализовать продукцию
        private void btnRealize_Click(object sender, EventArgs e)
        {
            if(dgvFoodNow.CurrentRow !=null)
            {
                double fCount = double.Parse(dgvFoodNow.CurrentRow.Cells[2].Value.ToString());
                int id = Int32.Parse(dgvFoodNow.CurrentRow.Cells[0].Value.ToString());
                FormConsider fConsider = new FormConsider(mdb, fCount, id);
                fConsider.ShowDialog();
                UpdateFoodParams();
            }
        }
        //Открыть управляющий терминал
        private void btnController_Click(object sender, EventArgs e)
        {
            FormManagment fManagment = new FormManagment(mdb);
            fManagment.ShowDialog();
        }
    }
}
