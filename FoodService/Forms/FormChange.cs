﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FoodService
{
    public partial class FormChange : Form
    {
        mdb mdb;
        int id;
        public FormChange(FoodParams foodParams, mdb _mdb)
        {
            InitializeComponent();
            mdb = _mdb;
            //Задать стартовые параметры
            id = foodParams.Id;
            tbName.Text = foodParams.Name;
            dtpDate.Value = foodParams.DateIn;
            dtpTime.Value = foodParams.DateIn;
            cbType.SelectedIndex = (int)foodParams.FoodType;
            tbCount.Text = foodParams.FoodCount.ToString();
            dtpRealize.Value = foodParams.RealizationTime;
            if(foodParams.DateEnd != new DateTime())
            {
                dtpDataEnd.Enabled = true;
                dtpDataEnd.Value = foodParams.DateEnd;
                dtpTimeEnd.Enabled = true;
                dtpTimeEnd.Value = foodParams.DateEnd;
            }
        }
        /// <summary>
        /// Внести изменения
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCreate_Click(object sender, EventArgs e)
        {
            FoodParams food = new FoodParams();
            food.Name = tbName.Text;
            food.Id = id;
            food.FoodCount = double.Parse(tbCount.Text);
            food.FoodType = (FoodType)cbType.SelectedIndex;
            food.DateIn = DateTime.Parse(string.Format("{0} {1}", dtpDate.Value.ToShortDateString(), 
                dtpTime.Value.ToShortTimeString()));
            if(dtpDataEnd.Enabled)
            {
                food.DateEnd = DateTime.Parse(string.Format("{0} {1}", dtpDataEnd.Value.ToShortDateString(),
                dtpTimeEnd.Value.ToShortTimeString()));
            }

            mdb.DoChangeParams(food);
            this.Close();
        }
    }
}
