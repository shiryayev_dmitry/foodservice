﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FoodService
{
    public partial class FormConsider : Form
    {
        //Проверка стоит ли кнопки реализовать все
        bool chFlag = false;
        //Количество продукции
        double foodCount;
        mdb mdb;
        int id;
        public FormConsider(mdb _mdb, double _foodCount, int _id)
        {
            InitializeComponent();
            mdb = _mdb;
            id = _id;
            foodCount = _foodCount;
            tbCountNow.Text = foodCount.ToString();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            //Заменить на противоположный
            checkBox1.Checked = !chFlag;
            //Изменить значение флага
            chFlag = !chFlag;
            if (checkBox1.Checked)
            {
                //Даём доступ к дате реализации
                dtpDate.Enabled = true;
                dtpTime.Enabled = true;
                tbCount.Text = foodCount.ToString();
            }
            else
            {
                //Закрываем доступ к дате реализации
                dtpDate.Enabled = false;
                dtpTime.Enabled = false;
                tbCount.Text = "";
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Считаем сколько продукции осталось на складе
            double food = foodCount - double.Parse(tbCount.Text);
            if (checkBox1.Checked || food == 0)
            {
                mdb.DoRealizeProduct(food, id, DateTime.Parse(string.Format("{0} {1}", dtpDate.Value.ToShortDateString(),
                    dtpTime.Value.ToShortTimeString())));
            }
            else
            {
                mdb.DoRealizeProduct(food, id);
            }
            this.Close();
        }

        private void tbCount_TextChanged(object sender, EventArgs e)
        {
            //Меняем значение сколько продукции осталось на складе
            if (!string.IsNullOrEmpty(tbCount.Text))
                tbCountNow.Text = (foodCount - double.Parse(tbCount.Text)).ToString();
            else
                tbCountNow.Text = foodCount.ToString();

            if(tbCountNow.Text == "0")
            {
                dtpDate.Enabled = true;
                dtpTime.Enabled = true;
            }
            else
            {
                dtpDate.Enabled = false;
                dtpTime.Enabled = false;
            }    
        }
    }
}
