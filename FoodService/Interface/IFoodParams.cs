﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodService
{
    public interface IFoodParams
    {
        string Name { get; set; }
        double FoodCount { get; set; }
        DateTime DateIn { get; set; }
        DateTime RealizationTime { get; set; }
        DateTime DateEnd { get; set; }   
    }
}
