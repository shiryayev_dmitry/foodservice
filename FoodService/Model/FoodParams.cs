﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodService
{
    public class FoodParams: IFoodParams
    {
        public int Id { get; set; }
        public string Name { get; set;}
        public double FoodCount { get; set; }
        public DateTime DateIn { get; set; }
        public DateTime RealizationTime { get; set; }
        public DateTime DateEnd { get; set; }
        public FoodType FoodType { get; set; }
    }

    public enum FoodType
    {
        killogramm = 0,
        piece = 1,
        litter = 2
    }
}
