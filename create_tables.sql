CREATE TABLE FoodService(
food_id AutoIncrement NOT NULL,
name Text(50) NOT NULL,
food_count Float,
date_in DATETIME NOT NULL,
realization_time DATETIME NOT NULL,
date_end DATETIME,
food_type INT
);